﻿using UnityEngine;
using System.Collections;

public class CarGamesHud : MonoBehaviour
{

    public GUISkin Skin;
    public Car player;

    public void OnGUI()
    {
        GUI.skin = Skin;

        GUILayout.BeginArea(new Rect(0, 0, Screen.width, Screen.height));
        {
            GUILayout.BeginVertical(Skin.GetStyle("CarGamesHud"));
            {
                GUILayout.Label("BAYMAX's JOURNEY", Skin.GetStyle("Name"));
                GUILayout.Label(string.Format("Points: {0}", CarGameManager.Instance.Points), Skin.GetStyle("PointsText"));
                GUILayout.Label(string.Format("Points To Pass THis Level: {0}", CarLevelFinish.points_to_pass), Skin.GetStyle("PointsToPassText"));

                var time = CarLevelManager.Instance.RunningTime;
                /*GUILayout.Label(string.Format(
                    "Time {0:00}:{1:00} Get {2} bonus",
                    time.Minutes + (time.Hours * 60),
                    time.Seconds,
                    CarLevelManager.Instance.CurrentTimeBonus), Skin.GetStyle("TimeText"));*/
            }
            GUILayout.EndVertical();
        }
        GUILayout.EndArea();
    }
}