﻿using UnityEngine;
using System.Collections;

public class Car : MonoBehaviour {

    private bool facingRight;
    private CarCharacterController2D _controller;
    private float normolizeHorizontalSpeed;

    public float MaxSpeed = 8;
    public float SpeedOnGround = 10f;
    public float SpeedOnAir = 5f;
    public int MaxHealth = 100;
    public GameObject OuchEffect;
    //public Projectiles Projectile;
    public float FireRate;
    public Transform ProjectileFireLocation;
    /*public AudioClip PlayerHitSound;
    public AudioClip PlayerShootSound;
    public AudioClip PlayerHealthSound;
    public AudioClip PlayerDeathSound;
    public Animator Animator;
    */

    //NEW
    WheelJoint2D[] wheelJoints;
    public Transform centerOfMass;
    JointMotor2D motorBack, motorFront;
    float dir = 0f;
    float torqueDir = 0f;
    float maxFwdSpeed = -5000;
    float maxBwdSpeed = 2000f;
    float accelerationRate = 600;
    float decelerationRate = -100;
    float brakeSpeed = 2500f;
    float gravity = 9.81f;
    float slope = 0;
    public Transform rearWheel;
    public Transform frontWheel;

    public Texture2D speedoMeterDial;
    public Texture2D speedoMeterPointer;

    public int Health { get; private set; }
    public bool IsDead { get; private set; }

    private float _canFireIn;

    public void Awake()
    {
        _controller = GetComponent<CarCharacterController2D>();
        //facingRight = transform.localScale.x > 0;
        Health = MaxHealth;

    }

    void Start()
    {
        rigidbody2D.centerOfMass = centerOfMass.transform.localPosition;
        wheelJoints = gameObject.GetComponents<WheelJoint2D>();
        motorBack = wheelJoints[0].motor;
        motorFront = wheelJoints[1].motor;
    }
    public void Update()
    {

        _canFireIn -= Time.deltaTime;
        if (!IsDead)
            HandleInput();

        //var movementFacor = _controller.State.IsGrounded ? SpeedOnGround : SpeedOnAir;

        /*if (IsDead)
            _controller.SetHorizontalForce(0);
        else
            _controller.SetHorizontalForce(Mathf.Lerp(_controller.Velocity.x, normolizeHorizontalSpeed * MaxSpeed, Time.deltaTime * movementFacor)); */
        //Animator.SetFloat("speed", Mathf.Abs(_controller.Velocity.x) / MaxSpeed);
        //Animator.SetBool("IsGrounded", _controller.State.IsGrounded);
    }

    public void FinishLevel()
    {
        enabled = false;
        //_controller.enabled = false;
        collider2D.enabled = false;
    }

    public void kill()
    {
        _controller.HandleCollision = false;
        collider2D.enabled = false;

        IsDead = true;
        Health = 0;
        _controller.SetForce(new Vector2(0, 16));
        //AudioSource.PlayClipAtPoint(PlayerDeathSound, transform.position);
    }
    public void RespawnAt(Transform spawnPoint)
    {
        /*if (!facingRight)
            Flip(); */
        IsDead = false;
        collider2D.enabled = true;
        //_controller.HandleCollision = true;
        Health = MaxHealth;

        transform.position = spawnPoint.position;
    }

    public void TakeDamage(int damage, GameObject instigator)
    {
        //AudioSource.PlayClipAtPoint(PlayerHitSound, transform.position);
        //FloatingText.Show(string.Format("-{0}!", damage), "DamageText", new FromWorldPointTextPositioner(Camera.main, transform.position, 1.5f, 60f));

        Instantiate(OuchEffect, transform.position, transform.rotation);
        Health -= damage;

        /*if (Health <= 0)
            LevelManager.Instance.KillPlayer(); */
    }

    public void GiveHealth(int health, GameObject instagator)
    {
        //FloatingText.Show(string.Format("Health Gained +{0}!!", health), "HealthText", new FromWorldPointTextPositioner(Camera.main, transform.position, 2.5f, 60f));
        Health = Mathf.Min(Health + health, MaxHealth);
        //AudioSource.PlayClipAtPoint(PlayerHealthSound, transform.position);
    }

    private void HandleInput()
    {
        torqueDir = Input.GetAxis("Horizontal");
        //mph = rigidbody2D.velocity.magnitude * 2.237;

        //mphDisplay.text = mph + " MPH";

        if (torqueDir != 0)
        {
            rigidbody2D.AddTorque(3 * Mathf.PI * torqueDir, ForceMode2D.Force);

        }
        else
        {
            rigidbody2D.AddTorque(0);
        }
        slope = transform.localEulerAngles.z;
        if (slope >= 180)
            slope = slope - 360;
        dir = Input.GetAxis("Horizontal");

        if (dir != 0)
            motorBack.motorSpeed = Mathf.Clamp(motorBack.motorSpeed - (dir * accelerationRate - gravity * Mathf.Sin((slope * Mathf.PI) / 180) * 80) * Time.deltaTime, maxFwdSpeed, maxBwdSpeed);
        motorFront.motorSpeed = Mathf.Clamp(motorFront.motorSpeed - (dir * accelerationRate - gravity * Mathf.Sin((slope * Mathf.PI) / 180) * 80) * Time.deltaTime, maxFwdSpeed, maxBwdSpeed);
        if ((dir == 0 && motorBack.motorSpeed < 0) || (dir == 0 && motorBack.motorSpeed == 0 && slope < 0))
        {
            motorBack.motorSpeed = Mathf.Clamp(motorBack.motorSpeed - (decelerationRate - gravity * Mathf.Sin((slope * Mathf.PI) / 180) * 80) * Time.deltaTime, maxFwdSpeed, 0);
            motorFront.motorSpeed = Mathf.Clamp(motorFront.motorSpeed - (decelerationRate - gravity * Mathf.Sin((slope * Mathf.PI) / 180) * 80) * Time.deltaTime, maxFwdSpeed, 0);
        }
        else if ((dir == 0 && motorBack.motorSpeed > 0) || (dir == 0 && motorBack.motorSpeed == 0 && slope > 0))
        {
            motorBack.motorSpeed = Mathf.Clamp(motorBack.motorSpeed - (-decelerationRate - gravity * Mathf.Sin((slope * Mathf.PI) / 180) * 80) * Time.deltaTime, 0, maxBwdSpeed);
            motorFront.motorSpeed = Mathf.Clamp(motorFront.motorSpeed - (-decelerationRate - gravity * Mathf.Sin((slope * Mathf.PI) / 180) * 80) * Time.deltaTime, 0, maxBwdSpeed);
        }



        if (Input.GetKey(KeyCode.Space) && motorBack.motorSpeed > 0)
        {
            motorBack.motorSpeed = Mathf.Clamp(motorBack.motorSpeed - brakeSpeed * Time.deltaTime, 0, maxBwdSpeed);
            motorFront.motorSpeed = Mathf.Clamp(motorFront.motorSpeed - brakeSpeed * Time.deltaTime, 0, maxBwdSpeed);

        }
        else if (Input.GetKey(KeyCode.Space) && motorBack.motorSpeed < 0)
        {
            motorBack.motorSpeed = Mathf.Clamp(motorBack.motorSpeed + brakeSpeed * Time.deltaTime, maxFwdSpeed, 0);
            motorFront.motorSpeed = Mathf.Clamp(motorFront.motorSpeed + brakeSpeed * Time.deltaTime, maxFwdSpeed, 0);
        }
        wheelJoints[0].motor = motorBack;
        wheelJoints[1].motor = motorFront;
        /*if (_controller.CanJump && Input.GetKeyDown(KeyCode.Space))
        {
            _controller.Jump();
        }*/
        if (Input.GetMouseButtonDown(0))
        {
            FireProjectile();
        }
    }

    private void FireProjectile()
    {
        if (_canFireIn > 0)
            return;

        var direction = facingRight ? Vector2.right : -Vector2.right;
        //var projectile = (Projectiles)Instantiate(Projectile, ProjectileFireLocation.position, ProjectileFireLocation.rotation);
        //projectile.Initialize(gameObject, direction, _controller.Velocity);
        //projectile.transform.localScale = new Vector3(facingRight ? 1 : -1, 1, 1);
        _canFireIn = FireRate;

        //AudioSource.PlayClipAtPoint(PlayerShootSound, transform.position);
        //Animator.SetTrigger("Fire");
    }

    private void Flip()
    {
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        facingRight = transform.localScale.x > 0;
    }

    public void OnGUI()
    {
        Rect rect = new Rect(Screen.width - 300, Screen.height - 150, 300, 150);
        GUI.DrawTexture(rect, speedoMeterDial);

        float CurrentSpeed = -rigidbody2D.velocity.x;
        float speedFactor = CurrentSpeed / maxFwdSpeed;
        float rotationAngle = Mathf.Lerp(0, 180, speedFactor * 15);

        GUIUtility.RotateAroundPivot(rotationAngle, new Vector2(Screen.width - 150, Screen.height));
        Rect rect1 = new Rect(Screen.width - 300, Screen.height - 150, 300, 300);
        GUI.DrawTexture(rect1, speedoMeterPointer);


    }
}
