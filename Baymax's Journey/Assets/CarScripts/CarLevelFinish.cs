﻿using UnityEngine;
using System.Collections;

public class CarLevelFinish : MonoBehaviour {

    public string NextLevelName;
    public string ResetLevel;
    public static int points_to_pass = 350;
    private static int pass = 0;

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Car>() == null)
            return;
        if (CarGameManager.Instance.Points >= points_to_pass)
        {
            pass = 1;
            CarLevelManager.Instance.GotoNextLevel(NextLevelName);
        }

        else
        {
            pass = 0;
            CarLevelManager.Instance.GotoNextLevel2(ResetLevel, points_to_pass);

        }
    }

    public static int CheckPass()
    {
        return pass;
    }
}
