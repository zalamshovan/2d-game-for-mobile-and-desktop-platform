﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
public class CarLevelManager : MonoBehaviour {

    public static CarLevelManager Instance { get; private set; }
    public Car Player { get; private set; }
    public CameraController Camera { get; private set; }
    public TimeSpan RunningTime { get { return DateTime.UtcNow - _started; } }

    public int CurrentTimeBonus
    {
        get
        {
            var secondDifference = (int)(BonusCutOfSeconds - RunningTime.TotalSeconds);
            return Mathf.Max(0, secondDifference) * BonusSecondMultiplier;
        }
    }

    private List<CarCheckPoint> _checkpoints;
    private int _currentCheckIndex;
    private DateTime _started;
    private int _savedPoints;

    public CarCheckPoint DebugSpawn;
    public int BonusCutOfSeconds;
    public int BonusSecondMultiplier;

    public void Awake()
    {

        if (CarLevelFinish.CheckPass() == 1)
        {
            _savedPoints = CarGameManager.Instance.Points;
        }
        else
            CarGameManager.Instance.Reset();


        Instance = this;

    }

    public void Start()
    {

        _checkpoints = FindObjectsOfType<CarCheckPoint>().OrderBy(t => t.transform.position.x).ToList();
        _currentCheckIndex = _checkpoints.Count > 0 ? 0 : -1;

        Player = FindObjectOfType<Car>();
        Camera = FindObjectOfType<CameraController>();

        _started = DateTime.UtcNow;

        var listeners = FindObjectsOfType<MonoBehaviour>().OfType<iCarRespawnListener>();
        foreach (var listener in listeners)
        {
            for (var i = _checkpoints.Count - 1; i >= 0; i--)
            {
                var distance = ((MonoBehaviour)listener).transform.position.x - _checkpoints[i].transform.position.x;
                if (distance < 0)
                    continue;

                _checkpoints[i].AssignObjectToCheckpoint(listener);
                break;
            }
        }

#if UNITY_EDITOR
        if (DebugSpawn != null)
        {
            DebugSpawn.SpawnPlayer(Player);
        }
        else if (_currentCheckIndex != -1)
        {
            _checkpoints[_currentCheckIndex].SpawnPlayer(Player);
        }
#else
        if(_currentCheckIndex != -1)
            _checkpoints[_currentCheckIndex].SpawnPlayer(Player);
#endif

    }

    public void Update()
    {

        var isAtLastCheck = _currentCheckIndex + 1 >= _checkpoints.Count;
        if (isAtLastCheck)
            return;

        var distanceToNextCheck = _checkpoints[_currentCheckIndex + 1].transform.position.x - Player.transform.position.x;
        if (distanceToNextCheck >= 0)
            return;

        _checkpoints[_currentCheckIndex].PlayerLeftCheckpoint();
        _currentCheckIndex++;
        _checkpoints[_currentCheckIndex].PlayerHitCheckPoint();

        CarGameManager.Instance.AddPoints(CurrentTimeBonus);
        _savedPoints = CarGameManager.Instance.Points;
        _started = DateTime.UtcNow;



    }

    public void GotoNextLevel(string levelName)
    {
        StartCoroutine(GotoNextLevelCo(levelName));
    }

    private IEnumerator GotoNextLevelCo(string levelname)
    {
        Player.FinishLevel();
        //GameManager.Instance.AddPoints(CurrentTimeBonus);
        CarFloatingText.Show(string.Format("Level Finished"), "CheckpointText", new CarCenteredTextPositioner(.20f));
        yield return new WaitForSeconds(1);
        CarFloatingText.Show(string.Format("{0} POINTS!!", CarGameManager.Instance.Points), "CheckpointText", new CarCenteredTextPositioner(.25f));


        yield return new WaitForSeconds(2f);

        if (string.IsNullOrEmpty(levelname))
            Application.LoadLevel("Menu");
        else
            Application.LoadLevel(levelname);
    }


    public void GotoNextLevel2(string levelName, int _points)
    {
        StartCoroutine(GotoNextLevelCo2(levelName, _points));
    }

    private IEnumerator GotoNextLevelCo2(string levelname, int _points)
    {
        Player.FinishLevel();
        //GameManager.Instance.AddPoints(CurrentTimeBonus);
        CarFloatingText.Show(string.Format("Level Failed!!"), "CheckpointText", new CarCenteredTextPositioner(.20f));
        yield return new WaitForSeconds(1);
        CarFloatingText.Show(string.Format("{0} MORE POINTS NEEDED", Mathf.Abs(_points - CarGameManager.Instance.Points)), "CheckpointText", new CarCenteredTextPositioner(.20f));
        yield return new WaitForSeconds(1);
        CarFloatingText.Show(string.Format("Level is RESTARTING"), "CheckpointText", new CarCenteredTextPositioner(.20f));

        yield return new WaitForSeconds(3f);

        if (string.IsNullOrEmpty(levelname))
            Application.LoadLevel("Menu");
        else
            Application.LoadLevel(levelname);
    }


    public void KillPlayer()
    {
        StartCoroutine(KillPlayerCo());
    }

    private IEnumerator KillPlayerCo()
    {
        Player.kill();
        //Camera.IsFollowing = false;
        yield return new WaitForSeconds(2f);

        //Camera.IsFollowing = true;

        if (_currentCheckIndex != -1)
            _checkpoints[_currentCheckIndex].SpawnPlayer(Player);

        _started = DateTime.UtcNow;
        CarGameManager.Instance.ResetPoints(_savedPoints);
    }
}
