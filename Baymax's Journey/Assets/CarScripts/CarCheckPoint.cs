﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class CarCheckPoint : MonoBehaviour {

    private List<iCarRespawnListener> _listeners;
    public void Awake()
    {
        _listeners = new List<iCarRespawnListener>();
    }

    public void PlayerHitCheckPoint()
    {
        StartCoroutine(PlayerHitCheckPointCo(CarLevelManager.Instance.CurrentTimeBonus));
    }

    private IEnumerator PlayerHitCheckPointCo(int bonus)
    {
        CarFloatingText.Show("Checkpoint!", "CheckpointText", new CarCenteredTextPositioner(.3f));
        yield return new WaitForSeconds(.3f);
        CarFloatingText.Show(string.Format("+{0} time bonus!", bonus), "Bonus", new CarCenteredTextPositioner(.2f));
    }

    public void PlayerLeftCheckpoint()
    {

    }
    public void SpawnPlayer(Car player)
    {
        player.RespawnAt(transform);

        foreach (var listener in _listeners)
            listener.onPlayerRespawnInThisCheckpoint(this, player);

    }
    public void AssignObjectToCheckpoint(iCarRespawnListener listener)
    {
        _listeners.Add(listener);
    }
}
