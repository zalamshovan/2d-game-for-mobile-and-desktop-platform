﻿using UnityEngine;
using System.Collections;

public class CarGameManager : MonoBehaviour {

	public static CarGameManager _instance;

	public static CarGameManager Instance
	{
		get{ return _instance ?? (_instance=new CarGameManager());}
	}
	
	public int Points {
		get;
		private set;
	}

    private CarGameManager()
	{
	}
	
	public void Reset () {
		Points = 0;
	}

	public void ResetPoints (int points) {
		Points = points;
	}

	public void AddPoints (int pointsToAdd) {
		Points += pointsToAdd;
	}
}
