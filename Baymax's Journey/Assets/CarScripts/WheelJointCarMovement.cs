﻿using UnityEngine;
using System.Collections;


public class WheelJointCarMovement : MonoBehaviour
{
    WheelJoint2D[] wheelJoints;
    public Transform centerOfMass;
    JointMotor2D motorBack,motorFront;
    float dir = 0f;
    float torqueDir = 0f;
    float maxFwdSpeed = -5000;
    float maxBwdSpeed = 2000f;
    float accelerationRate = 600;
    float decelerationRate = -100;
    float brakeSpeed = 2500f;
    float gravity = 9.81f;
    float slope = 0;
    public Transform rearWheel;
    public Transform frontWheel;

    public Texture2D speedoMeterDial;
    public Texture2D speedoMeterPointer;


    void Start()
    {
        rigidbody2D.centerOfMass = centerOfMass.transform.localPosition;
        wheelJoints = gameObject.GetComponents<WheelJoint2D>();
        motorBack = wheelJoints[0].motor;
        motorFront = wheelJoints[1].motor;
    }

    void Update()
    {
        torqueDir = Input.GetAxis("Horizontal");
        //mph = rigidbody2D.velocity.magnitude * 2.237;
        
        //mphDisplay.text = mph + " MPH";
        
        if (torqueDir != 0)
        {
            rigidbody2D.AddTorque(3 * Mathf.PI * torqueDir, ForceMode2D.Force);
           
        }
        else
        {
            rigidbody2D.AddTorque(0);
        }
        slope = transform.localEulerAngles.z;
if (slope >= 180)
            slope = slope - 360;
        dir = Input.GetAxis("Horizontal");

        if (dir != 0)
            motorBack.motorSpeed = Mathf.Clamp(motorBack.motorSpeed - (dir * accelerationRate - gravity * Mathf.Sin((slope * Mathf.PI) / 180) * 80) * Time.deltaTime, maxFwdSpeed, maxBwdSpeed);
            //motorFront.motorSpeed = Mathf.Clamp(motorFront.motorSpeed - (dir * accelerationRate - gravity * Mathf.Sin((slope * Mathf.PI) / 180) * 80) * Time.deltaTime, maxFwdSpeed, maxBwdSpeed);
        if ((dir == 0 && motorBack.motorSpeed < 0) || (dir == 0 && motorBack.motorSpeed == 0 && slope < 0))
        {
            motorBack.motorSpeed = Mathf.Clamp(motorBack.motorSpeed - (decelerationRate - gravity * Mathf.Sin((slope * Mathf.PI) / 180) * 80) * Time.deltaTime, maxFwdSpeed, 0);
            //motorFront.motorSpeed = Mathf.Clamp(motorFront.motorSpeed - (decelerationRate - gravity * Mathf.Sin((slope * Mathf.PI) / 180) * 80) * Time.deltaTime, maxFwdSpeed, 0);
        }
        else if ((dir == 0 && motorBack.motorSpeed > 0) || (dir == 0 && motorBack.motorSpeed == 0 && slope > 0))
        {
            motorBack.motorSpeed = Mathf.Clamp(motorBack.motorSpeed - (-decelerationRate - gravity * Mathf.Sin((slope * Mathf.PI) / 180) * 80) * Time.deltaTime, 0, maxBwdSpeed);
            //motorFront.motorSpeed = Mathf.Clamp(motorFront.motorSpeed - (-decelerationRate - gravity * Mathf.Sin((slope * Mathf.PI) / 180) * 80) * Time.deltaTime, 0, maxBwdSpeed);
        }



        if (Input.GetKey(KeyCode.Space) && motorBack.motorSpeed > 0)
        {
            motorBack.motorSpeed = Mathf.Clamp(motorBack.motorSpeed - brakeSpeed * Time.deltaTime, 0, maxBwdSpeed);
            //motorFront.motorSpeed = Mathf.Clamp(motorFront.motorSpeed - brakeSpeed * Time.deltaTime, 0, maxBwdSpeed);

        }
        else if (Input.GetKey(KeyCode.Space) && motorBack.motorSpeed < 0)
        {
            motorBack.motorSpeed = Mathf.Clamp(motorBack.motorSpeed + brakeSpeed * Time.deltaTime, maxFwdSpeed, 0);
            //motorFront.motorSpeed = Mathf.Clamp(motorFront.motorSpeed + brakeSpeed * Time.deltaTime, maxFwdSpeed, 0);
        }
        wheelJoints[0].motor = motorBack; 
        //wheelJoints[1].motor = motorFront;



     

    }


    
    public void OnGUI(){
        Rect rect = new Rect(Screen.width-300, Screen.height-150, 300, 150);
        GUI.DrawTexture(rect, speedoMeterDial);

        float CurrentSpeed = -rigidbody2D.velocity.x;
        float speedFactor = CurrentSpeed/maxFwdSpeed;
        float rotationAngle = Mathf.Lerp(0, 180, speedFactor*15);

        GUIUtility.RotateAroundPivot(rotationAngle, new Vector2(Screen.width - 150, Screen.height));
        Rect rect1 = new Rect(Screen.width - 300, Screen.height - 150, 300, 300);
        GUI.DrawTexture(rect1, speedoMeterPointer);


    }


}