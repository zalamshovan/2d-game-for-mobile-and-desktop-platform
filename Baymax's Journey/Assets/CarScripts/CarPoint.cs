﻿using UnityEngine;
using System.Collections;

public class CarPoint : MonoBehaviour, iCarRespawnListener
{

    public GameObject Effect;
    public int PointsToAdd = 10;

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Car>() == null)
            return;

        CarGameManager.Instance.AddPoints(PointsToAdd);
        Instantiate(Effect, transform.position, transform.rotation);

        gameObject.SetActive(false);
        CarFloatingText.Show(string.Format("+{0}!", PointsToAdd), "PointStarText", new CarFromWorldPointTextPositioner(Camera.main, transform.position, 1.5f, 50));

    }

    public void onPlayerRespawnInThisCheckpoint(CarCheckPoint checkpoint, Car player)
    {
        gameObject.SetActive(true);
    }

}
