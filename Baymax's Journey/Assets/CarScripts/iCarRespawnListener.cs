﻿public interface iCarRespawnListener
{
    void onPlayerRespawnInThisCheckpoint(CarCheckPoint checkpoint, Car player);
}