﻿using UnityEngine;
using System.Collections;

public class CarInstakill : MonoBehaviour
{
    public void OnTriggerEnter2D(Collider2D other)
    {
        var player = other.GetComponent<Car>();

        if (player == null)
        {
            return;
        }

        CarLevelManager.Instance.KillPlayer();
    }


}
