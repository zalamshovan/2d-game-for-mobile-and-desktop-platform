﻿using UnityEngine;
using System.Collections;

public class PathedProjectile : MonoBehaviour,ITakeDamage
{
    private Transform _destiniton;
    private float _speed;

    public GameObject destroyEffect;
    public int PointsToGiveToPlayer;

    public void Initialize(Transform destinition , float speed)
    {
        _destiniton = destinition;
        _speed = speed;
    }

    public void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, _destiniton.position, Time.deltaTime * _speed);

        var distanceSquared = (_destiniton.transform.position - transform.position).sqrMagnitude;
        if (distanceSquared > .01f * .01f)
            return;

        if (destroyEffect != null)
            Instantiate(destroyEffect, transform.position, transform.rotation);

        Destroy(gameObject);
    }


    public void TakeDamage(int damage, GameObject instigator)
    {
        if(destroyEffect!= null)
        {
            Instantiate(destroyEffect, transform.position, transform.rotation);
        }
        Destroy(gameObject);

        var projectile = instigator.GetComponent<Projectiles>();
        if(projectile!= null && projectile.Owner.GetComponent<Player>()!= null && PointsToGiveToPlayer!= 0)
        {
            GameManager.Instance.AddPoints(PointsToGiveToPlayer);
            FloatingText.Show(string.Format("+{0}!", PointsToGiveToPlayer), "PointStarText", new FromWorldPointTextPositioner(Camera.main, transform.position, 1.5f, 50));
        }
    }
}
