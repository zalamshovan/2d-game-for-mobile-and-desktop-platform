﻿using UnityEngine;
using System.Collections;

public class BasicEnemyAI : MonoBehaviour,ITakeDamage,iPlayerRespawnListener
{
    public float Speed;
    public float FireRate = 1;
    public Projectiles Projectile;
    public GameObject DestroyEffect;
    public int PointsToGiveToPlayer;

    private CharacterController2D _controller;
    private Vector2 _direction;
    private Vector2 _startPosition;
    private float _canFireIn;


    public void Start()
    {
        _controller = GetComponent<CharacterController2D>();
        _direction = new Vector2(-1, 0);
        _startPosition = transform.position;
    }

    public void Update()
    {
        _controller.SetHorizontalForce(_direction.x * Speed);

        if((_direction.x <0 && _controller.State.IsCollidingLeft) ||(_direction.x >0 && _controller.State.IsCollidingRight)  )
        {
            _direction = -_direction;
            transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        }

        if ((_canFireIn -= Time.deltaTime) > 0)
            return;

        var raycast = Physics2D.Raycast(transform.position, _direction, 10, 1 << LayerMask.NameToLayer("Player"));
        if (!raycast)
            return;

        var projectile = (Projectiles)Instantiate(Projectile, transform.position, transform.rotation);
        projectile.Initialize(gameObject, _direction, _controller.Velocity);
        _canFireIn = FireRate;

    }


    public void onPlayerRespawnInThisCheckpoint(CheckPoint checkpoint, Player player)
    {
        _direction = new Vector2(-1, 0);
        transform.localScale =new Vector3(1, 1, 1);
        transform.position = _startPosition;
        gameObject.SetActive(true);
    }

    public void TakeDamage(int damage, GameObject instigator)
    {
        if(PointsToGiveToPlayer!= 0 )
        {
            var projectile = instigator.GetComponent<Projectiles>();
            if (projectile != null && projectile.Owner.GetComponent<Player>() != null && PointsToGiveToPlayer != 0)
            {
                GameManager.Instance.AddPoints(PointsToGiveToPlayer);
                FloatingText.Show(string.Format("+{0}!", PointsToGiveToPlayer), "PointStarText", new FromWorldPointTextPositioner(Camera.main, transform.position, 1.5f, 50));
            }
        }
        Instantiate(DestroyEffect, transform.position, transform.rotation);
        gameObject.SetActive(false);
    }
}
