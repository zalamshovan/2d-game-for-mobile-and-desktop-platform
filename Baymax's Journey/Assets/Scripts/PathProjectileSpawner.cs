﻿using UnityEngine;
using System.Collections;

public class PathProjectileSpawner : MonoBehaviour 
{
    public Transform destination;
    public PathedProjectile Projectile;

    public GameObject SpawnEffect;
    public float Speed;
    public float FireRate;
    public Animator Animator;


    private float _nextShotInSecond;

    public void Start()
    {
        _nextShotInSecond = FireRate;
    }

    public void Update()
    {
        if ((_nextShotInSecond -= Time.deltaTime) > 0)
            return;

        _nextShotInSecond = FireRate;
        var projectile = (PathedProjectile)Instantiate(Projectile, transform.position, transform.rotation);
        projectile.Initialize(destination, Speed);

        if (SpawnEffect != null)
            Instantiate(SpawnEffect, transform.position, transform.rotation);

        if (Animator != null)
            Animator.SetTrigger("Fire");
    }

    public void OnDrawGizmos()
    {
        if (destination == null)
            return;
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, destination.position);
    }
	
}
