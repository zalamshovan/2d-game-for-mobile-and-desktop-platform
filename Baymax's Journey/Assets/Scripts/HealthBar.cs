﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour
{
    public Player player;
    public Transform ForegroundSprite;
    public SpriteRenderer ForegroundRenderer;
    public Color MaxHealthcol = new Color(255 / 255f, 63 / 255f, 63 / 255f);
    public Color MinHealthcol = new Color(64 / 255f, 137 / 255f, 255 / 255f);

	public void Update()
    {
        var healtParcentage = player.Health / (float)player.MaxHealth;
        ForegroundSprite.localScale = new Vector3(healtParcentage, 1, 1);
        ForegroundRenderer.color = Color.Lerp(MaxHealthcol, MinHealthcol, healtParcentage);

    }
}
