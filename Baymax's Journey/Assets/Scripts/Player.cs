﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour , ITakeDamage
{
    private bool facingRight;
    private CharacterController2D _controller;
    private float normolizeHorizontalSpeed;

    public float MaxSpeed =8;
    public float SpeedOnGround = 10f;
    public float SpeedOnAir = 5f;
    public int  MaxHealth = 100;
    public GameObject OuchEffect;
    public Projectiles Projectile;
    public float FireRate;
    public Transform ProjectileFireLocation;
    public AudioClip PlayerHitSound;
    public AudioClip PlayerShootSound;
    public AudioClip PlayerHealthSound;
    public AudioClip PlayerDeathSound;
    public Animator Animator;

    public int Health { get; private set; }
    public bool IsDead { get; private set; }

    private float _canFireIn;

    public void Awake()
    {
        _controller = GetComponent<CharacterController2D>();
        facingRight = transform.localScale.x > 0;
        Health = MaxHealth;
    }

    public void Update()
    {
        _canFireIn -= Time.deltaTime;
        if(!IsDead)
            HandleInput();

        var movementFacor = _controller.State.IsGrounded ? SpeedOnGround : SpeedOnAir;

        if (IsDead)
            _controller.SetHorizontalForce(0);
        else
            _controller.SetHorizontalForce(Mathf.Lerp(_controller.Velocity.x, normolizeHorizontalSpeed * MaxSpeed, Time.deltaTime * movementFacor));

        Animator.SetFloat("Speed", Mathf.Abs(_controller.Velocity.x) / MaxSpeed);
        Animator.SetBool("IsDead", IsDead);
    
    }

    public void FinishLevel()
    {
        enabled = false;
        _controller.enabled = false;
        collider2D.enabled = false;
    }

    public void kill()
    {
        _controller.HandleCollision = false;
        collider2D.enabled = false;

        IsDead = true;
        Health = 0;
        _controller.SetForce(new Vector2(0, 16));
        AudioSource.PlayClipAtPoint(PlayerDeathSound, transform.position);
    }
    public void RespawnAt(Transform spawnPoint)
    {
        if (!facingRight)
            Flip();
        IsDead = false;
        collider2D.enabled = true;
        _controller.HandleCollision = true;
        Health = MaxHealth;

        transform.position = spawnPoint.position;
    }

    public void TakeDamage(int damage, GameObject instigator)
    {
        AudioSource.PlayClipAtPoint(PlayerHitSound, transform.position);
        FloatingText.Show(string.Format("-{0}!", damage), "DamageText", new FromWorldPointTextPositioner(Camera.main, transform.position, 1.5f, 60f));

        Instantiate(OuchEffect, transform.position, transform.rotation);
        Health -= damage;

        if (Health <= 0)
            LevelManager.Instance.KillPlayer();
    }

    public void GiveHealth(int health,  GameObject instagator)
    {
        FloatingText.Show(string.Format("Health Gained +{0}!!", health), "HealthText", new FromWorldPointTextPositioner(Camera.main, transform.position, 2.5f, 60f));
        Health = Mathf.Min(Health + health, MaxHealth);
        AudioSource.PlayClipAtPoint(PlayerHealthSound, transform.position);
    }

    private void HandleInput()
    {
        if(Input.GetKey(KeyCode.D))
        {
            normolizeHorizontalSpeed = 1;
            if(!facingRight)
                Flip();
        }
        else if(Input.GetKey(KeyCode.A))
        {
            normolizeHorizontalSpeed=-1;
            if(facingRight)
                Flip();
        }
        else{
            normolizeHorizontalSpeed=0;
        }
        if(_controller.CanJump && Input.GetKeyDown(KeyCode.Space))
        {
            _controller.Jump();
        }
        if (Input.GetMouseButtonDown(0))
        {
            FireProjectile();
        }
    }

    private void FireProjectile()
    {
        if (_canFireIn > 0)
            return;

        var direction = facingRight ? Vector2.right : -Vector2.right;
        var projectile = (Projectiles)Instantiate(Projectile, ProjectileFireLocation.position, ProjectileFireLocation.rotation);
        projectile.Initialize(gameObject, direction, _controller.Velocity);
        //projectile.transform.localScale = new Vector3(facingRight ? 1 : -1, 1, 1);
        _canFireIn = FireRate;

        AudioSource.PlayClipAtPoint(PlayerShootSound, transform.position);
        Animator.SetTrigger("Fire");
    }

    private void Flip()
    {
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        facingRight = transform.localScale.x > 0;
    }
}
