﻿
using UnityEngine;
public class SimpleProjectile : Projectiles , ITakeDamage
{
    public int Damage;
    public GameObject DestroyEffect;
    public int PointsTOGiveToPlayer;
    public float TimetoLive;

    public void Update()
    {
        if((TimetoLive-=Time.deltaTime)<=0)
        {
            DestroyProjectile();
            return;
        }

        //transform.Translate((Direction + new Vector2(InitialVelocity.x,0))* Speed * Time.deltaTime, Space.World);
        transform.Translate(Direction * ((Mathf.Abs(InitialVelocity.x) + Speed) * Time.deltaTime), Space.World);
    }
    public void TakeDamage(int damage, GameObject instigator)
    {
        if (PointsTOGiveToPlayer != 0)
        {
            var projectile = instigator.GetComponent<Projectiles>();
            if(projectile!= null && projectile.Owner.GetComponent<Player>()!= null )
            {
                GameManager.Instance.AddPoints(PointsTOGiveToPlayer);
                FloatingText.Show(string.Format("+{0}!", PointsTOGiveToPlayer), "PointStarText", new FromWorldPointTextPositioner(Camera.main, transform.position, 1.5f,50));
            }
        }
        DestroyProjectile(); 
    }

    protected override void OnCollideOther(Collider2D other)
    {
        DestroyProjectile();
    }

    protected override void OnCollideTakeDamage(Collider2D other, ITakeDamage takeDamage)
    {
        takeDamage.TakeDamage(Damage, gameObject);
        DestroyProjectile();
    }

    private void DestroyProjectile()
    {
        if(DestroyEffect!=null)
        {
            Instantiate(DestroyEffect, transform.position, transform.rotation);
        }
        Destroy(gameObject);
    }
}

