﻿using UnityEngine;
using System.Collections;

public abstract class Projectiles : MonoBehaviour 
{
    public float Speed;
    public LayerMask CollisionMask;

    public GameObject Owner { get; private set; }
    public Vector2 Direction { get; private set; }
    public Vector2 InitialVelocity { get; private set; }

    public void Initialize(GameObject owner, Vector2 direction, Vector2 init )
    {
        transform.right= direction;
        Owner = owner;
        Direction = direction;
        InitialVelocity = init;
        OnInitialized();
    }

    protected virtual void OnInitialized()
    {

    }

    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        if((CollisionMask.value & (1<<other.gameObject.layer))==0)
        {
            OnNotCollidewith(other);
            return;
        }
        var isOwner = other.gameObject == Owner;
        if(isOwner)
        {
            OnCollideOwner();
            return;
        }

        var TakeDamage = (ITakeDamage)other.GetComponent(typeof(ITakeDamage));
        if(TakeDamage!= null)
        {
            OnCollideTakeDamage(other, TakeDamage);
            return;
        }
        OnCollideOther(other);

    }

    protected virtual void OnNotCollidewith(Collider2D other)
    {

    }
    protected virtual void OnCollideOwner()
    {

    }
    protected virtual void OnCollideTakeDamage(Collider2D other, ITakeDamage takeDamage)
    {

    }
    protected virtual void OnCollideOther(Collider2D other)
    {

    }

}
