﻿using UnityEngine;
using System.Collections;

public class LevelFinish : MonoBehaviour 
{

    public string NextLevelName;
    public string ResetLevel;
    public static int points_to_pass = 400;
    private static int pass=0;

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Player>() == null)
            return;
        if (GameManager.Instance.Points >= points_to_pass){
            pass = 1;
            LevelManager.Instance.GotoNextLevel(NextLevelName);
        }
            
        else
        {
            pass = 0;
            LevelManager.Instance.GotoNextLevel2(ResetLevel, points_to_pass);

        }
    }
	
    public static int CheckPass()
    {
        return pass;
    }

}
