﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CheckPoint : MonoBehaviour
{
    private List<iPlayerRespawnListener> _listeners;
    public void Awake()
    {
        _listeners = new List<iPlayerRespawnListener>();
    }

    public void PlayerHitCheckPoint()
    {
        StartCoroutine(PlayerHitCheckPointCo(LevelManager.Instance.CurrentTimeBonus));
    }

    private IEnumerator PlayerHitCheckPointCo(int bonus)
    {
        FloatingText.Show("Checkpoint!", "CheckpointText", new CenteredTextPositioner(.3f));
        yield return new WaitForSeconds(.3f);
        FloatingText.Show(string.Format("+{0} time bonus!",bonus), "Bonus", new CenteredTextPositioner(.2f));
    }

    public void PlayerLeftCheckpoint()
    {

    }
    public void SpawnPlayer(Player player)
    {
        player.RespawnAt(transform);

        foreach (var listener in _listeners)
            listener.onPlayerRespawnInThisCheckpoint(this, player);

    }
	public void AssignObjectToCheckpoint(iPlayerRespawnListener listener)
    {
        _listeners.Add(listener);
    }

}
